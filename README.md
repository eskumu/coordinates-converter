# L-Est97 to WGS84 coordinates converter

> Simple application for converting L-Est97 to WGS84 coordinates.
This application has GUI, but it can also be used programmatically.

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/eskumu/coordinates-converter.svg) ![PyPI - Python Version](https://img.shields.io/pypi/pyversions/coordinates-converter.svg) ![PyPI](https://img.shields.io/pypi/v/coordinates-converter.svg)

Runs on Python 2.7 and Python 3

## Installation

Application is hosted in [PyPI](https://pypi.org/project/coordinates-converter/) and can be installed via pip:

`pip install coordinates-converter`

## Usage

To launch GUI application write the following command to the terminal.

`coordinates-app`

If users want to convert coordinates in the code, then simple api is provided:

```python
from coordinates.converter import CoordinateConverter, WGS84, L_Est97

converter = CoordinateConverter

# converting to L-Est97
wgs_point = WGS84(lat=59.39528, long=24.664104611385)
print(converter.wgs84_to_l_est97(wgs_point))

# converting to WGS84
est97_point = L_Est97(x=6543210.14, y=543210.86)
print(converter.l_est97_to_wgs84(est97_point))

# module can also convert decimal degrees to degree-minute-second system
from coordinates.converter import convert_decimal_to_degrees, convert_degrees_to_decimal

print(convert_decimal_to_degrees(10.51))
print(convert_degrees_to_decimal(10, 30, 36.0))

# L-Est97 system has coordinate boundaries where it can be used.
# These boundaries can be checked with validator
from coordinates.converter import LEst97CoordinatesValidator
validator = LEst97CoordinatesValidator()
print(validator.validate_projected_x(6543210.14))
print(validator.validate_projected_y(543210.86))
print(validator.validate_wgs84_latitude(59.39528))
print(validator.validate_wgs84_longitude(21.09))

```

## Documentation

[Documentation can be found here](https://eskumu.gitlab.io/coordinates-converter)

## Developer guide.

### Installation

Sourcecode can be found in [GitLab](https://gitlab.com/eskumu/coordinates-converter/)

For building the app from source, run 

`pip install -editable .`

### Tests 

Tests are written in [pytest](https://docs.pytest.org/en/latest/) 
and for running tests pytest is needed.

To install pytest run:

`pip install pytest`

To execute test suite:
`python -m pytest tests/`

### Code Quality

Continuous integration environment checks that code complies with PEP8 checcks.
For testing code quality [flake8](http://flake8.pycqa.org/en/latest/) is used.

To install flake8 tests it must be installed:

`pip install flake8`

Flake8 tests can be run with following command:

`flake8 coordinates/`

---
Links:

- [PyPI](https://pypi.org/project/coordinates-converter/)
- [Gitlab repository](https://gitlab.com/eskumu/coordinates-converter)
- [Documentation](https://eskumu.gitlab.io/coordinates-converter)
- [Task specification](https://enos.itcollege.ee/~eikivi/python/hw1.txt)
