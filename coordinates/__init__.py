"""
# L-Est97 to WGS84 coordinates converter

Simple application for converting L-Est97 to WGS84 coordinates.

You can find usage and installation instructions in the
[readme](https://gitlab.com/eskumu/coordinates-converter/blob/master/README.md).

Application logic in `coordinates.converter` submodule and
Client app is in `coordinates.client` submodule.
"""


name = 'coordinate_converter'
__pdoc__ = {}  # initialize pdoc object.
